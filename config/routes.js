/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/
  // router khusus apihhhhhhh
  
  // note
  // auth by jwt tidak diimplementasikan sebagai middleware (at sails sih police...), jadi tidak perlu login untuk menjalankan api... xixixi
  // answe api
  'GET /v1/answer/biodata': { action: 'answer/biodata' },
  'GET /v1/answer/json_restapi': { action: 'answer/json-is' },
  
  // user api
  'POST /v1/auth': { action: 'user/auth' },
  'POST /v1/user/register': { action: 'user/user' },
  
  // pruduct
  'POST /v1/product/create': { action: 'product/create' },
  'GET /v1/product': { action: 'product/list' },
  'PUT /v1/product/update': { action: 'product/update' },
  'DELETE /v1/product/:id': { action: 'product/delete' },
  'POST /v1/product/filter': { action: 'product/filter-count' },


};
