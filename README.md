# Author : Mukmin littaqwa

[![N|Solid](https://sailsjs.com/images/logos/sails-logo_ltBg_dkBlue.png)](https://sailsjs.com/)

### noted :

> menggunakan sails.js untuk mempersingkat waktu test (sedang reaksi KIPI pasca vaksin)
> akan tetapi jika ingin hasil test menggunakan express.js / node.js native akan saya kerjakan
> ,jika terdapat kendala dalam running code, bisa menghubungi.. terimakasih

### Sails v.1.2.3

### Node js v14.17.6 (use node version manager)

module :

- JWT (jsonwebtoken)
- sails-hook-apianalytics
- sails-hook-organics
- sails-mysql

jawaban pertanyaan terdapat dalam documentasi API by postman bersama dengan
dokumentasi RestApi 😇😇😇

```sh
https://documenter.getpostman.com/view/14243840/U16qH2uj
```

## Installation

import terlebih dahulu knowlege_test.SQL (menggunakan mysql)

Install the dependencies and devDependencies and start the server.

```sh
cd knowledge_test
npm i --save
npm run serve atau sails lift
```

note

```sh
untuk auto migration set pada config/models.js (migrate: "alter")
-
untuk auto set pada database terdapat pada config/datastores.js (use sails-mysql)
mysql://root:@localhost:3306/knowledge_test (//user:password@address:port/my_db)
```

## License

MIT

**Free Software, Hell Yeah!**

[//]: # "These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax"
[dill]: https://github.com/joemccann/dillinger
[git-repo-url]: https://github.com/joemccann/dillinger.git
[john gruber]: http://daringfireball.net
[df1]: http://daringfireball.net/projects/markdown/
[markdown-it]: https://github.com/markdown-it/markdown-it
[ace editor]: http://ace.ajax.org
[node.js]: http://nodejs.org
[twitter bootstrap]: http://twitter.github.com/bootstrap/
[jquery]: http://jquery.com
[@tjholowaychuk]: http://twitter.com/tjholowaychuk
[express]: http://expressjs.com
[angularjs]: http://angularjs.org
[gulp]: http://gulpjs.com
[pldb]: https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md
[plgh]: https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md
[plgd]: https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md
[plod]: https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md
[plme]: https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md
[plga]: https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md
