module.exports = {


  friendlyName: 'Biodata',


  description: 'Biodata answer.',


  inputs: {

  },


  exits: {
     success: {
        statusCode: 200,
        description: 'ini dia biodatanya.'
      },
  },


  fn: async function (inputs,exits) {
    
    var biodata = {
      nama_lengkap: "Mukmin littaqwa",
      nama_panggilan : "taqwa atau wawa atau wak...😅",
      ttl: {
        tempat: "tembilahan, riau",
        tgl: "17-07-1995 🥳🥳🥳🥳🥳"
      },
      warganegara: "wakwakland 😱😱😱",
      alamat_domisili: "Jln.plosokuning raya, Gg. Kutilang III, No.28, Ngaglik, Sleman, Yogyakarta",
      motto_hidup: "semua terlihat tergantung dari akun alter yang ada di twitter, apakah sebagai kucing atau *sebagian text hilang*",
      sosial_media: {
        twitter : "https://twitter.com/mochacat6",
        instagram: "@taqwa17"
      },
      hobby: "berbicara dengan hewan, kadang disangka atau dikatain gila padahal sedang linking awareness... 😭",
      
    }
    
    return exits.success(biodata);

  }


};
