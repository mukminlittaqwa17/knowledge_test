module.exports = {


  friendlyName: 'Auth',


  description: 'Auth user.',


  inputs: {
     email: {
      type: 'string',
      required: true,
      description: 'email'
    },

    password: {
      type: 'string',
      required: true,
      description: 'password'
    },
  },


  exits: {
    success: {
        description: 'sukses deh pokok e.'
      },

      notFound: {
        statusCode: 403,
        description: 'not found'
     },

     notInsert: {
        statusCode: 400,
        description: 'data tidak berhasil'
     }
  },


  fn: async function (inputs, exits) {

   let data = await User.findOne({email: inputs.email.toLowerCase()})
    .meta({enableExperimentalDeepTargets:true})
    
    if (!data) {
      
      return exits.notFound({message: 'email tidak terdaftar'});
      
    } else {
      
      inputs.password = await sails.helpers.passwords.checkPassword(inputs.password, data.password)
      
      let user = await User.findOne({email: inputs.email.toLowerCase(),'password': inputs.password})
      .meta({enableExperimentalDeepTargets:true})
      
      if (!user) {
        
        return exits.notFound({message: 'password salah'});
        
      } else {
        
        let token = await sails.helpers.jwtAuth(user)
        
        login = {
                id: user.id,
                fullname: user.fullname,
                email: user.email,
                token: token,
              }
              
        return exits.success({login,message: 'success'});
        
      }
      
    }

  }


};
