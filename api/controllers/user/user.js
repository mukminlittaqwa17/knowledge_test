module.exports = {


  friendlyName: 'User',


  description: 'user regist',


  inputs: {
    
    fullname: {
      type: 'string',
      required: true,
      description: 'nama lengkap'
    },
    
    bird: {
      type: 'string',
      required: true,
      description: 'tempat & tanggal lahir'
    },
    
    dom: {
      type: 'string',
      required: true,
      description: 'domisili'
    },
    
    address: {
      type: 'string',
      required: true,
      description: 'Alamat lengkap'
    },
    
    phonenumber: {
      type: 'string',
      required: true,
      description: 'nomor Hp'
    },
    
    email: {
      type: 'string',
      required: true,
      description: 'email'
    },
    
     password: {
      type: 'string',
      required: true,
      description: 'password'
    },
    
  },


  exits: {
    success: {
        description: 'New user account was created successfully.'
      },

      notFound: {
        statusCode: 403,
        description: 'not found'
    }
  },


  fn: async function (inputs, exits) {

    // hashing password
    inputs.password = await sails.helpers.passwords.hashPassword(inputs.password)
    
    const user = await User.create(inputs).fetch().then((user) => {
      
     sails.log(user)

     return exits.success({message: 'success', user});

    }).catch((err) => {
      
      return exits.notFound({error: true ,message: 'wah gawat.... error ini tuh....', err});

    });

  }


};
