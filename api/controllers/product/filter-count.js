module.exports = {


  friendlyName: 'Filter count',


  description: '',


  inputs: {
     price_filter: {
      type: 'string',
      required: true,
      description: 'price filter & count'
    },
  },


  exits: {
    success: {
        statusCode: 200,
        description: 'berhasil filter product.'
      },

      notFound: {
        statusCode: 403,
        description: 'not found'
    }
  },


  fn: async function (inputs, exits) {
    
    let product = await Product.find({
        price: {
          '>': inputs.price_filter
        }
      }).then((product) => {

          if (product) {

            let count = product.length
            
            const payload = {
             filter: inputs.price_filter,
             total_product: count,
             list_product: product
            }
            return exits.success({payload});

          } else {

            return exits.notFound({message: 'tidak ada list product'});

          }
          

          }).catch((err) => {

            return exits.notFound({error: true , message: 'error ini tuh', err});

          });
      
      
      

  }


};
