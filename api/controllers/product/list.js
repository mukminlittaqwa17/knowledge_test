module.exports = {


  friendlyName: 'List',


  description: 'List product.',


  inputs: {

  },


  exits: {
     success: {
        statusCode: 200,
        description: 'ini dia datanya.'
      },

      notFound: {
        statusCode: 403,
        description: 'data tidak ada'
    }
  },


  fn: async function (inputs, exits) {

    const product= await Product.find().then((product) => {

    if (product) {


      return exits.success({product});

    } else {

      return exits.notFound({message: 'tidak ada list product'});

    }
    

    }).catch((err) => {

      return exits.notFound({error: true , message: 'error ini tuh', err});

    });

  }


};
