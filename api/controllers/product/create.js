module.exports = {


  friendlyName: 'Create',


  description: 'Create product.',


  inputs: {
    name_product: {
      type: 'string',
      required: true,
      description: 'nama product'
    },
    
    price: {
      type: 'number',
      required: true,
      description: 'harga'
    },
    
    Qty: {
      type: 'number',
      required: true,
      description: 'jumlah product'
    },
  },


  exits: {
      success: {
        description: 'New user account was created successfully.'
      },

      notFound: {
        statusCode: 403,
        description: 'not found'
    }
  },


  fn: async function (inputs, exits) {

    const product = await Product.create(inputs).fetch().then((product) => {
      
     sails.log(product)

     return exits.success({message: 'success', product});

    }).catch((err) => {
      
      return exits.notFound({error: true ,message: 'wah gawat.... error ini tuh....', err});

    });

  }


};
