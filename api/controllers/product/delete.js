module.exports = {


  friendlyName: 'Delete',


  description: 'Delete product.',


  inputs: {

  },


  exits: {
    success: {
        statusCode: 200,
        description: 'berhasil hapus ini tuh.'
      },

      notFound: {
        statusCode: 403,
        description: 'not found'
    }
  },


  fn: async function (inputs,exits) {

    let id = this.req.param('id')

    // delete product
    var product = await Product.destroyOne({ id:id }).then((product) => {
      
      if (product) {

        return exits.success({message: 'success delete product', product});

      } else {

        return exits.notFound({message: `tidak ada prodoct dengan id : ${id}`});
        
      }
    
    }).catch((err) => {

        return exits.notFound({error: true ,message: 'error ini tuh', err});

    });

  }


};
