module.exports = {


  friendlyName: 'Update',


  description: 'Update product.',


  inputs: {
    
    product_id: {
      type: 'string',
      required: true,
      description: 'product_id'
    },
    
    name_product: {
      type: 'string',
      // required: true,
      description: 'nama product'
    },
    
    price: {
      type: 'number',
      // required: true,
      description: 'harga'
    },
    
    Qty: {
      type: 'number',
      // required: true,
      description: 'jumlah product'
    },
  },


  exits: {
      success: {
        description: 'update data product berhasil.'
      },

      notFound: {
        statusCode: 403,
        description: 'not found'
    }
  },


  fn: async function (inputs, exits) {

    const id = inputs.product_id

    delete inputs.product_id 

    const product = await Product.update({ id : id },inputs).fetch()
    .then((product) => {
      
     sails.log(product)

     return exits.success({message: 'success update data product', inputs});

    }).catch((err) => {
      
      sails.log(product)
      
      return exits.notFound({error: true ,message: 'error ini tuh', err});

    });

  }


};
