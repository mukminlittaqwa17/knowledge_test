module.exports = {


  friendlyName: 'Jwt',


  description: 'enkripsi payload token nya dulu yak....',


  inputs: {
    payload: {
      type: 'ref',
      description: 'Berisi object payload data user untuk dienkripsi'
    },
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {
    
    const jwt = require('jsonwebtoken');
    
    const payload = inputs.payload
    

    return await jwt.sign({payload}, sails.config.credentials.jwtSecret, {
      expiresIn: '365d'
    })
    
  }


};

